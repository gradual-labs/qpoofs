#

self=$(readlink -m $0)
gistdir=$(dirname $self)

tic=$(date +%s%N | cut -c-13)
find . -name "*~1" -delete
export IPFS_PATH=$HOME/.../ipfs/usb/FILED

if [ -e $gistdir/qm.log.ots ]; then
  ots upgrade $gistdir/qm.log.ots
  cp -p $gistdir/qm.log $gistdir/qm.previous.log
  mv $gistdir/qm.log.ots $gistdir/qm.previous.log.ots
  git add qm.previous.log qm.previous.log.ots qm.log.ots.bak
  mtime=$(stat -c '%Y' qm.previous.log)
  qm=$(tail -1 qm.previous.log| cut -d' ' -f2)
  git commit -m "proof $qm as of $(date -d @$mtime)"
fi

if echo $1 | grep -q -e '^Qm'; then
  echo $tic: $1 >> $gistdir/qm.log
else
  qm=$(ipfs add -n -r $@ -Q)
  echo $tic: $qm >> $gistdir/qm.log
fi

cd $gistdir
openssl sha256 -r qm.log
sha=$(openssl sha256 -r qm.log | cut -d' ' -f1)
rm -f qm.log.ots
ots stamp qm.log
ots info qm.log.ots | tee qm.log.nfo

mtime=$(stat -c '%Y' qm.log)
git add qm.log qm.log.ots qm.log.nfo
git commit -a -m "$sha existed on $(date -d @$mtime)"

exit $?
1; # vim: syntax=sh
